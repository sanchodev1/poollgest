﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using PoollGest.Models;

namespace PoollGest.Areas.Identity.Data;

// Add profile data for application users by adding properties to the Colaborador class
public class Colaborador : IdentityUser
{
    [Key] [Display(Name = "ID do Colaborador")] public int ColaboradorId { get; set; }
    public int Id_colaborador { get; set; }

    [Display(Name = "Permissao")]
    public int? Id_permissao { get; set; }
    
    [PersonalData]
    [Required]
    [Display(Name = "Nome")]
    public String Nome { get; set; }

    [Display(Name = "É Administrador?")]
    public bool Is_admin { get; set; }
    [Display(Name = "Permissao")]
    public Permissao? Permissao { get; set; }
}

