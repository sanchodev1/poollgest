﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PoollGest.Data;
using PoollGest.Models;

namespace PoollGest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ColaboradorModelsController : ControllerBase
    {
        private readonly PoollGestContext _context;

        public ColaboradorModelsController(PoollGestContext context)
        {
            _context = context;
        }

        // GET: api/ColaboradorModels
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ColaboradorModel>>> GetColaboradorModel()
        {
            return await _context.ColaboradorModel.ToListAsync();
        }

        // GET: api/ColaboradorModels/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ColaboradorModel>> GetColaboradorModel(int id)
        {
            var colaboradorModel = await _context.ColaboradorModel.FindAsync(id);

            if (colaboradorModel == null)
            {
                return NotFound();
            }

            return colaboradorModel;
        }

        // PUT: api/ColaboradorModels/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutColaboradorModel(int id, ColaboradorModel colaboradorModel)
        {
            if (id != colaboradorModel.ColaboradorId)
            {
                return BadRequest();
            }

            _context.Entry(colaboradorModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ColaboradorModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ColaboradorModels
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<ColaboradorModel>> PostColaboradorModel(ColaboradorModel colaboradorModel)
        {
            _context.ColaboradorModel.Add(colaboradorModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetColaboradorModel", new { id = colaboradorModel.ColaboradorId }, colaboradorModel);
        }

        // DELETE: api/ColaboradorModels/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteColaboradorModel(int id)
        {
            var colaboradorModel = await _context.ColaboradorModel.FindAsync(id);
            if (colaboradorModel == null)
            {
                return NotFound();
            }

            _context.ColaboradorModel.Remove(colaboradorModel);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ColaboradorModelExists(int id)
        {
            return _context.ColaboradorModel.Any(e => e.ColaboradorId == id);
        }
    }
}
