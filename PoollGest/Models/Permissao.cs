﻿using System.ComponentModel.DataAnnotations;

namespace PoollGest.Models
{
    public class Permissao
    {
        [Key] [Display(Name = "ID da Permissao")] public int PermissaoId { get; set; }
        public int Id_permissao;
        [Display(Name = "Nome")]
        public String Nome;
    }
}
