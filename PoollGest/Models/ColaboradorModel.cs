﻿using System.ComponentModel.DataAnnotations;
namespace PoollGest.Models
{
    public class ColaboradorModel
    {
        [Key] [Display(Name = "ID do Colaborador")] public int ColaboradorId { get; set; }
        public int Id_colaborador { get; set; }

        [Display(Name = "Permissao")]
        public int? Id_permissao { get; set; }

        [Display(Name = "Nome")]
        public String Nome { get; set; }

        [Display(Name = "É Administrador?")]
        public bool Is_admin { get; set; }
        [Display(Name = "Permissao")]
        public Permissao? Permissao { get; set; }
    }
}
